from pathlib import Path

PROJECT_ROOT = Path(__file__).parents[1]
DATASETS = PROJECT_ROOT / "2021" / "data" / "year=2021" / "month=4" / "day=15" / "server=amsterdam1.ns.prodng.dc3.be"
DATASET = PROJECT_ROOT / "2021" / "data" / "year=2021" / "month=4" / "day=15" / "server=amsterdam1.ns.prodng.dc3.be" / "20210415-amsterdam1.ns.prodng.dc3.be-1c999c5d-30d6-440e-8845-e322040f2999.parq"
BAD_DOMAIN = [PROJECT_ROOT / "data" / "bad_domain" / "zeus_dga_domains.txt"]
GOOD_DOMAIN = PROJECT_ROOT / "data" / 'good_domain.txt'
QUERIES_DATASET = PROJECT_ROOT / "output" / "dataset_queries.csv"
QNAMES_DATASET = PROJECT_ROOT / "output" / "dataset_qnames.csv"
SRC_DATASET = PROJECT_ROOT / "output" / "dataset_src.csv"

PCAP = PROJECT_ROOT / "data" / "trace_dns_20170919_g - Copie.pcap"
NAMUR_CSV = PROJECT_ROOT / "data" / "trace_dns_20170919_g - Copie.csv"
NAMUR_QNAMES = PROJECT_ROOT / "data" / "Namur_qnames.csv"
NAMUR_REQUEST = PROJECT_ROOT / "data" / "Namur_request.csv"
