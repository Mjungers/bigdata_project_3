import os

from Dataset.load import load_parquet
from src.paths import DATASET, DATASETS, GOOD_DOMAIN


def getDuplicatesWithCount(listOfElems):
    ''' Get frequency count of duplicate elements in the given list '''
    dictOfElems = dict()
    # Iterate over each element in list
    for elem in listOfElems:
        # If element exists in dict then increment its value else add it in dict
        if elem in dictOfElems:
            dictOfElems[elem] += 1
        else:
            dictOfElems[elem] = 1
    return dictOfElems


def get_first_list():
    """
    get the domain name that appear at least 200 time in the dataset and save it to a file
    :return: None
    """
    df = load_parquet(DATASET)
    # compute_features(df)
    test = list(df["domainname"].values)

    dict1 = getDuplicatesWithCount(test)
    sorted_keys = sorted(dict1, key=dict1.get)  # [1, 3, 2]
    sorted_dict = {}
    for w in sorted_keys:
        sorted_dict[w] = dict1[w]
    total = 0
    nbr = 0
    with open("good_domain.txt", "w") as f:
        for key, value in sorted_dict.items():
            if value > 200:
                total += value
                nbr += 1
                if key != None:
                    f.write(key + ":" + str(value) + "\n")


def compute_all():
    """
    compute the list of top seen damonain name in the csv file and save it ito a file
    :return: None
    """
    files = os.listdir(DATASETS)
    file_result = open(GOOD_DOMAIN, 'r')
    lines = file_result.readlines()
    result_dict = {}
    for line in lines:
        data = line.split(":")
        result_dict[data[0]] = int(data[1])
    for f in files:
        df = load_parquet(DATASETS / f)
        # compute_features(df)
        test = list(df["domainname"].values)
        dict1 = getDuplicatesWithCount(test)
        sorted_keys = sorted(dict1, key=dict1.get)  # [1, 3, 2]
        sorted_dict = {}
        for w in sorted_keys:
            sorted_dict[w] = dict1[w]
        test_list = []
        for key, value in sorted_dict.items():
            if key in result_dict:
                result_dict[key] = result_dict[key] + value
                test_list.append(key)
        temp_dict = {}
        for item in test_list:
            temp_dict[item] = result_dict[item]
        result_dict = temp_dict
        print(len(result_dict))
    with open(GOOD_DOMAIN, "w") as newfile:
        for key, value in result_dict.items():
            newfile.write(key + ":" + str(value) + "\n")


def get_list():
    """
    get the list of the top domain in the dns Belgium files
    :return: the list of domain name
    """
    file_result = open(GOOD_DOMAIN, 'r')
    lines = file_result.readlines()
    result = []
    for line in lines:
        data = line.split(":")
        result.append(data[0])
    return result
