import pandas as pd

from paths import BAD_DOMAIN, GOOD_DOMAIN


def load_bad_domain():
    """
    this function is used to load the list of bad domain name
    :return: the list of domain name
    """
    list_domain = []
    for file in BAD_DOMAIN:
        list_domain += open(file).readlines()
    return list_domain


def load_good_domain():
    """
    this function is used to load the list of top domain name recovered in the dns_belgium dataset
    :return: the list of domain name
    """
    f = open(GOOD_DOMAIN)
    list_domain = []
    for line in f.readlines():
        list_domain.append(line.split(":")[0])
    return list_domain


def load_dataset(dataset_name):
    """
    this function is used to load the dataset from the csv
    :param dataset_name:
    :return:
    """
    return pd.read_csv(dataset_name)
