import pandas as pd

from Dataset.load import load_dataset
from paths import QUERIES_DATASET, QNAMES_DATASET, SRC_DATASET


def load_parquet(file_dir):
    return pd.read_parquet(file_dir)


def load_qnames_dataset():
    queries_features = load_dataset(QUERIES_DATASET)
    qnames_feature = load_dataset(QNAMES_DATASET)
    src_features = load_dataset(SRC_DATASET)
    temp_columns = ["src_" + name if name != 'src' else name for name in src_features.columns]
    src_features.columns = temp_columns
    temp = pd.merge(queries_features, src_features, on='src')
    agg_dict = {}

    for col in ['rcode-1', 'rcode0', 'rcode1', 'rcode2', 'rcode3', 'rcode4', 'rcode5',
                'rcode6', 'rcode7', 'rcode8', 'rcode9', 'countryRU', 'countryNG', 'countryBY']:
        agg_dict[col] = ['sum']
    for col in ['good_domain', 'domainname', 'req_len', 'res_len', 'qname_ratio_char_int', 'src_mean_ttl',
                'src_std_ttl',
                'src_count', 'src_time_min', 'src_time_max', 'src_nbr_qname', 'src_min_time_diff', 'src_max_time_diff',
                'src_mean_time_diff', 'src_std_time_diff', 'src_delta_time']:
        agg_dict[col] = ['mean', 'std']
    temp_dataset = temp.groupby(["qname"]).agg(agg_dict)
    temp = pd.merge(temp_dataset, qnames_feature, on='qname')
    print(temp.columns)
    temp.drop(columns=['rcode_list'], inplace=True)
    return temp.fillna(0)


def load_queries_dataset():
    qnames_feature = load_dataset(QNAMES_DATASET)
    queries_features = load_dataset(QUERIES_DATASET)
    temp = pd.merge(queries_features, qnames_feature, on='qname')
    src_features = load_dataset(SRC_DATASET)
    src_features.columns = ["src" + name for name in src_features.columns if name != 'src']
    temp = pd.merge(temp, src_features, on='src')
    temp.drop(columns=['qname', 'Unnamed: 0', 'rcode_list', 'src'], inplace=True)
    return temp.fillna(0)
