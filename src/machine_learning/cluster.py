import matplotlib.pyplot as plt
import numpy as np
from numpy.random import permutation
from sklearn.cluster import KMeans, DBSCAN
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler

from Dataset.load import load_dataset
from machine_learning.PCA_plot import show_3D
from paths import NAMUR_REQUEST


def cluster_KMeans(dataset, n_cluster):
    """
    compute the n cluster in the dataset
    :param dataset: the dataset
    :param n_cluster: the number of cluster to produce
    :return: the labels
    """
    result = KMeans(n_clusters=n_cluster, random_state=0).fit_predict(dataset)

    return result


def DBSCAN_algo(dataset):
    """
    use to the dbscan algorithm to produce clusters
    :param dataset: the dataset
    :return: a list with the labels
    """
    y = DBSCAN(eps=0.5, min_samples=5).fit_predict(dataset)
    return y


def TSNE_algo(dataset, labels):
    """
    Execute the tsne algorithm on the dataset and plot the result
    :param dataset: the dataset on which it is computed
    :param labels: a list coresponding to the labels of the data
    :return: None
    """
    t = permutation(100000)
    cluster = [line for line, label in zip(dataset, labels) if label == 1]
    dataset = np.concatenate((dataset[t, :], cluster), axis=0)
    y = np.concatenate((labels[t], (len(cluster) * [1])), axis=0)
    fig, ax = plt.subplots(1, 1, figsize=(15, 15), dpi=300)

    method = TSNE(n_components=2, random_state=0, n_jobs=6)
    Y = method.fit_transform(dataset)
    print("showing result")
    ax.scatter(Y[:, 0], Y[:, 1], c=y)
    plt.show()


if __name__ == "__main__":

    df = load_dataset(NAMUR_REQUEST).fillna(0)
    df_data = df[["id", "q_name"]]
    #select the final set of features
    df_temp = df[['id', 'nbr_ip', 'nbr_ip6', 'nbr_ip4', 'rcode:0', 'rcode:3',
        'q_name_ratio_char_int',
        'q_name_nbr_int', 'nb_rcode:3','%_rcode:3','localhost','len_q_name','nbr_split_q_name', 'mean_time_diff',
        'std_time_diff', 'nb_unique_ip', 'delta_ts_usec',
        'diff_ttls_nbr', 'mean_n_qname_unique','in_good_domain','reserved_ip']]
    identifier = list(df_temp.pop("id"))


    print("computing cluster")
    n_cluster = 3
    X = StandardScaler().fit_transform(df_temp)
    y = cluster_KMeans(X, n_cluster)


    df_data["label"] = y
    #print the various clusters
    for i in range(n_cluster):
        print(df_data[df_data.label == i])
    print("swhowing tsne")
    TSNE_algo(X,y)
    # end tsne

    #we tried oultier detection but did not gave results
    # outlier_detect = IsolationForest(contamination=0.001,n_jobs=4)
    # y = outlier_detect.fit_predict(df)

    print("plotting")
    show_3D(df_temp, y)


    #save the labeled data into a csv
    df["label"] = y
    df.to_csv("result.csv")
