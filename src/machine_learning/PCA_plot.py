import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import decomposition


def show_3D(X, y):
    """
    Plot the 3D representation of the cluster dataset using PCA algorithm
    :param X: the data features matrix
    :param y: the data label
    :return: None
    """

    plt.cla()
    pca = decomposition.PCA(n_components=3)

    X = pca.fit_transform(X)
    print(pca.components_)
    fig = plt.figure(1, figsize=(8, 6))
    plt.clf()
    ax = Axes3D(fig, elev=48, azim=134)
    ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=y,
               cmap=plt.cm.Set1)
    ax.set_title("First three PCA directions")
    ax.set_xlabel("1st eigenvector")
    ax.w_xaxis.set_ticklabels([])
    ax.set_ylabel("2nd eigenvector")
    ax.w_yaxis.set_ticklabels([])
    ax.set_zlabel("3rd eigenvector")
    ax.w_zaxis.set_ticklabels([])
    plt.show()


def plot_two_collumns(df, X, Y, labels=None):
    """
    this plot the two collumns of the dataframe into a scatter plot
    :param df: the dataframe to be ploted
    :param X: the name of the collumn for the X axes
    :param Y: the name of the collumn for the Y axes
    :param labels: a list of labels to collor the point
    :return: None
    """
    x = df.pop(X)
    y = df.pop(Y)
    if labels != None:
        plt.scatter(x, y, c=labels)
    else:
        plt.scatter(x, y)
    plt.show()


if __name__ == "__main__":
    print(None)
