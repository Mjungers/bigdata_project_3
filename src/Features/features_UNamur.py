import numpy as np
import pandas as pd
from numpy.core import mean

from Dataset.compute_removed import get_list
from paths import NAMUR_CSV, NAMUR_QNAMES, \
    NAMUR_REQUEST


def ratio_unique(serie):
    """
   compute the ratio of unique ip contained in the groupby series
   :param serie:  the series produce by the groupby(q_name) on the dataframe
   :return:  the ratio of unique ip
   """
    res = []
    for x in list(serie):
        if x != "[]":
            x = x.strip("[").strip("]")
            res += x.split(",")
    if len(res) == 0:
        return 0
    return len(set(res)) / len(res)


def unique_ip_per_qname(serie):
    """
    compute the numper of unique ip contained in the groupby series
    :param serie:  the series produce by the groupby(q_name) on the dataframe
    :return:  the number of unique ip
    """
    res = []
    for x in list(serie):
        if x != "[]":
            x = x.strip("[").strip("]")
            res += x.split(",")
    return len(set(res))


def compute_ips(row):
    """
    get the list of ip from the string contained in the datframe
    :param row: a row a the datframe
    :return: the list of ips
    """
    if row["A_AAA"] != "[]":
        x = row["A_AAA"].strip("[").strip("]")
        result = x.split(",")

        return result
    return None


def compute_ttl_features(serie):
    """
    this is used to compute the various ttl features on the dataset
    :param serie: the series that is passed in argument by the agregate
    :return: the list that contains the various features
    """
    res = []
    for x in list(serie):
        if x != "[]":
            x = x.strip("[").strip("]")
            res += x.split(",")
    res = [int(item) for item in res if item]
    # print(res)
    if res:
        nbr_diff_ttls = len(set(res))
        average = mean(res)
        std_dev = np.std(res)
        res.append(average)
        res.append(std_dev)
        res.append(nbr_diff_ttls)

    else:
        res.append("NaN")
        res.append("NaN")
        res.append(0)
    return res


def compute_ip_features(df, on='identifier'):
    """
    compute the mean, std of different q_name per ip and its ratio.
    :param df: the datafram on wich it is computed
    :param on: on which columns it should be grouped
    :return: a dataframe with the result
    """
    if on != "q_name":
        temp_df = df.loc[:, ('q_name', 'A_AAA', on)]
    else:
        temp_df = df.loc[:, ('q_name', 'A_AAA')]
    temp_df['ip'] = temp_df.apply(compute_ips, axis=1)
    ip_df = temp_df.explode('ip')
    ip_res_df = ip_df.groupby('ip').agg({'q_name': ['nunique', 'count']})
    ip_res_df.columns = ["n_qname_unique", "count_q_name"]
    ip_res_df["n_qname_ratio"] = ip_res_df["n_qname_unique"] / ip_res_df["count_q_name"]
    temp_df = ip_df.merge(ip_res_df, on='ip', how='left')
    result = temp_df.groupby(on).agg({"n_qname_unique": ["mean", "std"], "n_qname_ratio": ["mean"]})
    result.columns = ['mean_n_qname_unique', 'qtd_qname_unique', 'mean_ration_q_name']
    return result


def in_good_list(row, good_domain):
    """
    check if the q_name of a row in a is in the gooddomain list
    :param row:
    :param good_domain:
    :return:
    """
    q_name = row["q_name"]
    temp = str(q_name).split('.')
    if len(temp) >= 2:
        domain = temp[-2] + '.' + temp[-1]
        if domain in good_domain:
            return True
    return False


RESERVED_IPV4 = ["'0.", "'10.", "'100.", "'127.", "'169.254.", "'172.168.", "'192.0.0.", "'192.168."]
RESERVED_IPV6 = ["'fc", "'fd", "'fe80", "'ff"]
RESERVED_IP = RESERVED_IPV4 + RESERVED_IPV6


def compute_queries_features(csv_path):
    """
    compute the features specific to request on the dataset provided by unamur
    :param csv_path: the path to the dataset
    :return: the dataset with computed features
    """
    df = pd.read_csv(csv_path)

    df['id'] = df.index
    print(df.shape)

    q_name_df = pd.read_csv(NAMUR_QNAMES)

    q_name_feature = q_name_df.loc[:, ('q_name', 'nb_rcode:3', '%_rcode:3', 'ts_usec_min',
                                       'ts_usec_max', 'min_time_diff', 'max_time_diff', 'mean_time_diff',
                                       'std_time_diff', 'ratio_unique_ip', 'nb_unique_ip', 'delta_ts_usec',
                                       'diff_ttls_nbr', 'ttls_std', 'ttls_mean')]
    q_name_feature['len_q_name'] = q_name_df[['q_name']].apply(lambda x: len(str(x)))
    q_name_feature['nbr_split_q_name'] = q_name_df[['q_name']].apply(lambda x: len(str(x).split('.')))
    q_name_feature['localhost'] = q_name_df[['q_name']] == "localhost"

    good_domain = get_list()

    q_name_feature['in_good_domain'] = q_name_df[['q_name']].apply(in_good_list, good_domain=good_domain, axis=1)

    df = df.merge(q_name_feature, on='q_name', how='left')
    print(df.shape)

    ip_features_df = compute_ip_features(df, 'id')
    df['reserved_ip'] = df.A_AAA.str.contains("|".join(RESERVED_IP))

    initial_feature = df[["id", "q_name", "aa_flag", "tc_flag", "rd_flag", "ra_flag", "nbr_ip",
                          "nbr_ip6", "nbr_ip4", "mean_rdlenght", "std_rdlenght", "mean_ttl",
                          "std_ttl", "rcode:0", "rcode:1", "rcode:2", "rcode:3", "rcode:4",
                          "rcode:5", "questions_count", "answers_count", "authority_count", "additional_count",
                          "q_name_ratio_char_int", "q_name_nbr_int", "q_type_A", "q_type_AAAA", "q_type_ALIAS",
                          "q_type_CNAME",
                          "q_type_MX", "q_type_NS", "q_type_PTR", "q_type_SOA", "q_type_SRV", "q_type_TXT",
                          'nb_rcode:3', '%_rcode:3', 'ts_usec_min',
                          'ts_usec_max', 'min_time_diff', 'max_time_diff', 'mean_time_diff',
                          'std_time_diff', 'ratio_unique_ip', 'nb_unique_ip', 'delta_ts_usec',
                          'diff_ttls_nbr', 'ttls_std', 'ttls_mean', 'len_q_name', 'nbr_split_q_name', 'localhost',
                          'in_good_domain', 'reserved_ip']]
    print(initial_feature.shape)
    initial_feature = initial_feature.merge(ip_features_df, on='id', how='left')
    print(initial_feature.shape)
    return initial_feature


def compute_qnames_features(csv_path):
    """
    compute the features for each q_names in the Namur dataset of the specific csv file
    :param csv_path: the path to the csv file
    :return: a dataframe with the computed feaetures
    """
    df = pd.read_csv(csv_path)
    qname_df = df
    ip_features_df = compute_ip_features(df, 'q_name')

    qtype_count = qname_df.groupby("q_name").agg(
        dict(zip(("q_type_A", "q_type_AAAA", "q_type_ALIAS", "q_type_CNAME",
                  "q_type_MX", "q_type_NS", "q_type_PTR", "q_type_SOA", "q_type_SRV", "q_type_TXT",
                  "rcode:0", "rcode:1", "rcode:2", "rcode:3", "rcode:4", "rcode:5"), [["sum", "mean"]] * 16)))

    qtype_count.columns = ["nb_q_type_A", "%_q_type_A", "nb_q_type_AAAA", "%_q_type_AAAA", "nb_q_type_ALIAS",
                           "%_q_type_ALIAS", "nb_q_type_CNAME", "%_q_type_CNAME",
                           "nb_q_type_MX", "%_q_type_MX", "nb_q_type_NS", "%_q_type_NS", "nb_q_type_PTR",
                           "%_q_type_PTR", "nb_q_type_SOA", "%_q_type_SOA", "nb_q_type_SRV",
                           "%_q_type_SRV", "nb_q_type_TXT", "%_q_type_TXT",
                           "nb_rcode:0", "%_rcode:0", "nb_rcode:1", "%_rcode:1", "nb_rcode:2", "%_rcode:2",
                           "nb_rcode:3", "%_rcode:3", "nb_rcode:4", "%_rcode:4", "nb_rcode:5", "%_rcode:5"]

    # qaa: question,answers,authority
    qaa_stats = qname_df.groupby("q_name").agg(
        dict(zip(("questions_count", "answers_count", "authority_count"), [["sum", "mean", "std"]] * 3)))
    qaa_stats.columns = ["total_q_count", "mean_q_count", "std_q_count",
                         "total_ans_count", "mean_ans_count", "std_ans_count",
                         "total_auth_count", "mean_auth_count", "std_auth_count"]

    qname_df['time_diff'] = qname_df.groupby(["q_name"], sort=False)['ts_usec'].diff()
    ts_stats = qname_df.groupby("q_name").agg(
        {"ts_usec": ["min", "max"], 'time_diff': ['min', 'max', 'mean', 'std'],
         "A_AAA": [ratio_unique, unique_ip_per_qname]})
    ts_stats.columns = ["ts_usec_min", "ts_usec_max", 'min_time_diff', 'max_time_diff', 'mean_time_diff',
                        'std_time_diff', "ratio_unique_ip", "nb_unique_ip"]
    ts_stats["delta_ts_usec"] = ts_stats.ts_usec_max - ts_stats.ts_usec_min

    ttls_stats = qname_df.groupby("q_name").agg({"ttls": compute_ttl_features})
    ttls_stats["diff_ttls_nbr"] = ttls_stats.ttls.apply(lambda row: row[-1])
    ttls_stats["ttls_std"] = ttls_stats.ttls.apply(lambda row: row[-2])
    ttls_stats["ttls_mean"] = ttls_stats.ttls.apply(lambda row: row[-3])
    del ttls_stats["ttls"]

    qtype_count = qtype_count.merge(qaa_stats, on='q_name', how='left')
    qtype_count = qtype_count.merge(ts_stats, on='q_name', how='left')
    qtype_count = qtype_count.merge(ttls_stats, on='q_name', how='left')
    qtype_count = qtype_count.merge(ip_features_df, on='q_name', how='left')

    return qtype_count


if __name__ == "__main__":
    # first compute q_names features and save it
    compute_qnames_features(NAMUR_CSV).to_csv(NAMUR_QNAMES)
    # then compute the queries feature with this information
    compute_queries_features(NAMUR_CSV).to_csv(NAMUR_REQUEST)
