import pandas as pd

BLACKLISTED_DOMAINNAMES = ["be.", "brussels.", "vlaanderen.", "."]
BLACKLISTED_COUNTRIES = ["RU", "NG", "BY"]


def far_from_mean(dataframe, description, field):
    """
    If the ttl followed a normal law, then the probability than a ttl is in
    [mean - standard deviation ; mean + standard deviation] is 68.27%.
    """
    mean, std = description.get(field)
    return dataframe.get(field).between(mean - std, mean + std)


def compute_numerical_ratio(row):
    """
    Compute the various ratio on the q_name of the request for a row of a dataframe
    :param row:  the row of the dataset
    :return: the ratio of char and int in the q_name
    """
    qname = row['qname']
    number = list(filter(str.isnumeric, qname))
    if len(qname) > 0:  # len > 0
        ratio_char_int = len(number) / len(qname)
    else:
        ratio_char_int = 0
    return ratio_char_int


def get_features(dataset_name):
    """
    this computes the basic feature for each request in the DNS belgium dataset
    :param dataset_name: the path to the dataset
    :return: a dataframe with all the features
    """
    dataframe_in = pd.read_parquet(dataset_name)
    statistics = ["mean", "std"]
    description = dataframe_in.agg(dict(zip(("ttl", "req_len", "res_len"),
                                            [statistics] * 3)))
    dataframe_out = pd.DataFrame(dataframe_in.domainname.isin(BLACKLISTED_DOMAINNAMES))
    dataframe_out['good_domain'] = pd.DataFrame(dataframe_in.domainname.isin(list_gooddomain))
    dataframe_out = dataframe_out.join(far_from_mean(dataframe_in, description, "ttl"))
    dataframe_out = dataframe_out.join(far_from_mean(dataframe_in, description, "req_len"))
    dataframe_out = dataframe_out.join(far_from_mean(dataframe_in, description, "res_len"))
    dataframe_out['qname_ratio_char_int'] = dataframe_in.apply(lambda x: compute_numerical_ratio(x), axis=1)
    dataframe_out['qname'] = dataframe_in.qname
    dataframe_out['src'] = dataframe_in.src
    for rcode in range(-1, 10):
        dataframe_out = dataframe_out.join(dataframe_in.rcode.eq(rcode))
        dataframe_out = dataframe_out.rename({"rcode": f"rcode{rcode}"}, axis=1)
    for country in BLACKLISTED_COUNTRIES:
        dataframe_out = dataframe_out.join(dataframe_in.country.eq(country))
        dataframe_out = dataframe_out.rename({"country": f"country{country}"}, axis=1)
    dataframe_out = dataframe_out.join(dataframe_in.srcp.lt(1024))
    return dataframe_out


def compute_qname_features(dataframe):
    """
    compute the various features coresponding to each q_name
    :param dataframe: the dns belgium dataframe computed with the get features function
    :return: a dataframe with all the features
    """
    dataframe.sort_values(['qname', 'time'], inplace=True)
    dataframe['time_diff'] = dataframe.groupby(["qname"], sort=False)['time'].diff()
    result = dataframe.groupby(["qname"]).agg(
        {'qname': ['count'], 'labels': ['first'], 'ttl': ['mean', 'std'], 'time': ['min', 'max'], 'rcode': 'unique',
         'src': 'nunique', 'time_diff': ['min', 'max', 'mean', 'std'], })
    result.columns = ['count', 'labels', 'mean_ttl', 'std_ttl', 'time_min', 'time_max', 'rcode_list', 'nbr_source',
                      'min_time_diff', 'max_time_diff', 'mean_time_diff', 'std_time_diff']
    result['delta_time'] = result['time_max'] - result['time_min']

    return result


def compute_src_features(dataframe):
    """
    compute the features for each specific src in the dataset
    :param dataframe: the dns belgium dataframe computed with the get features function
    :return: a dataframe with all the features
    """
    dataframe.sort_values(['src', 'time'], inplace=True)
    dataframe['time_diff'] = dataframe.groupby(["src"], sort=False)['time'].diff()
    result = dataframe.groupby(["src"]).agg(
        {'src': ['count'], 'labels': ['first'], 'ttl': ['mean', 'std'], 'time': ['min', 'max'], 'rcode': 'unique',
         'qname': 'nunique', 'time_diff': ['min', 'max', 'mean', 'std'], })
    result.columns = ['count', 'labels', 'mean_ttl', 'std_ttl', 'time_min', 'time_max', 'rcode_list', 'nbr_qname',
                      'min_time_diff', 'max_time_diff', 'mean_time_diff', 'std_time_diff']
    result['delta_time'] = result['time_max'] - result['time_min']
    return result


if __name__ == "__main__":
    """
    list_gooddomain = load_good_domain()
    dataframe = pd.read_parquet(DATASET)
    dataframe_in = dataframe.copy()
    features = get_features(dataframe_in)
    features.to_csv(QUERIES_DATASET)
    dataframe_in = dataframe.copy()
    qname_features = compute_qname_features(dataframe_in)
    qname_features.to_csv(QNAMES_DATASET)
    dataframe_in = dataframe.copy()
    src_features = compute_src_features(dataframe_in)
    src_features.to_csv(SRC_DATASET)"""
